﻿Imports System.Data.SqlClient
Public Class Cekin
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM cekin"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID"
        DataGridView1.Columns(1).HeaderText = "ID Pemesanan"
        DataGridView1.Columns(2).HeaderText = "Nama Pemesan"
        DataGridView1.Columns(3).HeaderText = "Tanggal Cek IN"

        DataGridView1.Columns(0).Width = 200
        DataGridView1.Columns(1).Width = 200
        DataGridView1.Columns(2).Width = 195
        DataGridView1.Columns(3).Width = 185
    End Sub
    Sub bersih()
        txtid.Text = ""
        txtidpesan.Text = ""
        txtnama.Text = ""
        tglcekin.Text = ""
    End Sub
    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_cekin FROM cekin"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_cekin").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtid.Text = "CN-00" + Trim(Str(kodeauto))
            Case 2 : txtid.Text = "CN-00" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub
    Sub id_pemesanan()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM pemesan", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtidpesan.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtidpesan.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub

    Private Sub txtidpesan_TextChanged(sender As Object, e As EventArgs) Handles txtidpesan.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM pemesan WHERE id_pemesan = '" & txtidpesan.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            txtnama.Text = rd.Item("nama_pemesan")
        Else
        End If
        rd.Close()
        cn.Close()
    End Sub

    Private Sub Cekin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_22_P2;Integrated Security=True"
        tampildata()
        txtid.Enabled = False
        kodeotomatis()
        id_pemesanan()
        txtnama.Enabled = False

    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtid.Text = DataGridView1.SelectedCells(0).Value
        txtidpesan.Text = DataGridView1.SelectedCells(1).Value
        txtnama.Text = DataGridView1.SelectedCells(2).Value
        tglcekin.Value = DataGridView1.SelectedCells(3).Value
    End Sub

    Private Sub Simpan_Click(sender As Object, e As EventArgs) Handles Simpan.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Cekin , tidak boleh dikosongkan")
        ElseIf txtidpesan.Text = "" Then
            MessageBox.Show("ID Pemesanan tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtidpesan.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO cekin VALUES ('" & txtid.Text & "','" & txtidpesan.Text & "','" & txtnama.Text & "','" & tglcekin.Value.ToString("yyyy-MM-dd") & " ')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Cek IN Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Ubah_Click(sender As Object, e As EventArgs) Handles Ubah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Cek IN diisi, tidak boleh dikosongkan")
        ElseIf txtidpesan.Text = "" Then
            MessageBox.Show("ID Pemesanan wajib diisi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtidpesan.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE cekin SET id_pemesanan ='" & txtidpesan.Text & "',nama_pemesan ='" & txtnama.Text & "', tgl_cekin ='" & tglcekin.Value.ToString("yyyy-MM-dd") & "' WHERE id_cekin ='" & txtid.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Cek IN Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Hapus_Click(sender As Object, e As EventArgs) Handles Hapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM cekin WHERE id_cekin ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Cek IN Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub Batal_Click(sender As Object, e As EventArgs) Handles Batal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub Keluar_Click(sender As Object, e As EventArgs) Handles Keluar.Click
        Me.Close()
        FrmMenu.Show()
    End Sub

    Private Sub txtcari_TextChanged(sender As Object, e As EventArgs) Handles txtcari.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM cekin WHERE id_cekin LIKE '%" & txtcari.Text & "%' OR id_pemesanan LIKE '%" & txtcari.Text & "%' OR nama_pemesan LIKE '%" & txtcari.Text & "%' OR tgl_cekin LIKE '%" & txtcari.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub

   
    Private Sub Laporan_Click(sender As Object, e As EventArgs) Handles Laporan.Click
        Dim cekin As New Laporanin
        Laporanin.Show()
    End Sub
End Class