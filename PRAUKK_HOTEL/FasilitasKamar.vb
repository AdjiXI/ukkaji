﻿Imports System.Data.SqlClient
Public Class FasilitasKamar
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM fasilitas_kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID"
        DataGridView1.Columns(1).HeaderText = "ID Kamar"
        DataGridView1.Columns(2).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(3).HeaderText = "Fasilitas"

        DataGridView1.Columns(0).Width = 200
        DataGridView1.Columns(1).Width = 200
        DataGridView1.Columns(2).Width = 195
        DataGridView1.Columns(3).Width = 185
    End Sub
    Sub bersih()
        txtid.Text = ""
        txtidkamar.Text = ""
        txtnama.Text = ""
        txtjml.Text = ""
    End Sub
    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_fasilitas_kamar FROM fasilitas_kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_fasilitas_kamar").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtid.Text = "FK-00" + Trim(Str(kodeauto))
            Case 2 : txtid.Text = "FK-00" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub
    Sub id_kamar()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM kamar", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtidkamar.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtidkamar.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub
    Private Sub FasilitasKamar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_22_P2;Integrated Security=True"
        tampildata()
        txtid.Enabled = False
        kodeotomatis()
        id_kamar()
        txtnama.Enabled = False
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtid.Text = DataGridView1.SelectedCells(0).Value
        txtidkamar.Text = DataGridView1.SelectedCells(1).Value
        txtnama.Text = DataGridView1.SelectedCells(2).Value
        txtjml.Text = DataGridView1.SelectedCells(3).Value
    End Sub

    Private Sub txtidkamar_TextChanged(sender As Object, e As EventArgs) Handles txtidkamar.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM kamar WHERE id_kamar = '" & txtidkamar.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            txtnama.Text = rd.Item("tipe_kamar")
        Else
        End If
        rd.Close()
        cn.Close()
    End Sub

    Private Sub Simpan_Click(sender As Object, e As EventArgs) Handles Simpan.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Masuk , tidak boleh dikosongkan")
        ElseIf txtidkamar.Text = "" Then
            MessageBox.Show("ID Barang tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtidkamar.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO fasilitas_kamar VALUES ('" & txtid.Text & "','" & txtidkamar.Text & "','" & txtnama.Text & "','" & txtjml.Text & " ')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Fasilitas Kamar Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Ubah_Click(sender As Object, e As EventArgs) Handles Ubah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID wajib diisi, tidak boleh dikosongkan")
        ElseIf txtidkamar.Text = "" Then
            MessageBox.Show("ID Kamar wajib diisi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtidkamar.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE fasilitas_kamar SET id_kamar ='" & txtidkamar.Text & "',tipe_kamar ='" & txtnama.Text & "', fasilitas ='" & txtjml.Text & "' WHERE id_fasilitas_kamar ='" & txtid.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Fasilitas Kamar Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Hapus_Click(sender As Object, e As EventArgs) Handles Hapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM fasilitas_kamar WHERE id_fasilitas_kamar ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Fasilitas Kamar Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub Batal_Click(sender As Object, e As EventArgs) Handles Batal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub txtcari_TextChanged(sender As Object, e As EventArgs) Handles txtcari.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM fasilitas_kamar WHERE id_fasilitas_kamar LIKE '%" & txtcari.Text & "%' OR tipe_kamar LIKE '%" & txtcari.Text & "%' OR id_kamar LIKE '%" & txtcari.Text & "%' OR fasilitas LIKE '%" & txtcari.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub


    Private Sub Keluar_Click(sender As Object, e As EventArgs) Handles Keluar.Click
        Me.Close()
        FrmMenu.Show()
    End Sub
End Class