﻿
Imports System.Data.SqlClient
Public Class FasilitasHotel
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM fasilitas_hotel"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID "
        DataGridView1.Columns(1).HeaderText = "Fasilitas"
        DataGridView1.Columns(2).HeaderText = "Keterangan"

        DataGridView1.Columns(0).Width = 150
        DataGridView1.Columns(1).Width = 425
        DataGridView1.Columns(2).Width = 215
    End Sub
    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_hotel FROM fasilitas_hotel"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_hotel").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtid.Text = "H-00" + Trim(Str(kodeauto))
            Case 2 : txtid.Text = "H-00" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub
    Sub bersih()
        txtid.Text = ""
        txtnama.Text = ""
        txtket.Text = ""
    End Sub
    Private Sub FasilitasHotel_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_22_P2;Integrated Security=True"
        tampildata()
        txtid.Enabled = False
        kodeotomatis()
    End Sub

    Private Sub Simpan_Click(sender As Object, e As EventArgs) Handles Simpan.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Hotel, tidak boleh dikosongkan")
        ElseIf txtnama.Text = "" Then
            MessageBox.Show("Nama Fasilitas di isi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtnama.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO fasilitas_hotel VALUES ('" & txtid.Text & "','" & txtnama.Text & "','" & txtket.Text & " ')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Fasilitas Hotel Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Ubah_Click(sender As Object, e As EventArgs) Handles Ubah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Hotel, tidak boleh dikosongkan")
        ElseIf txtnama.Text = "" Then
            MessageBox.Show("Nama Fasilitas wajib di isi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtnama.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE fasilitas_hotel SET fasilitas ='" & txtnama.Text & "', keterangan ='" & txtket.Text & "' WHERE id_hotel = '" & txtid.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Fasilitas Hotel Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Hapus_Click(sender As Object, e As EventArgs) Handles Hapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM fasilitas_hotel WHERE id_hotel = '" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Fasilitas Hotel Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub
    Private Sub Batal_Click(sender As Object, e As EventArgs) Handles Batal.Click
        bersih()
        kodeotomatis()
    End Sub
    Private Sub Keluar_Click(sender As Object, e As EventArgs) Handles Keluar.Click
        Me.Close()
        FrmMenu.Show()
    End Sub
    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtid.Text = DataGridView1.SelectedCells(0).Value
        txtnama.Text = DataGridView1.SelectedCells(1).Value
        txtket.Text = DataGridView1.SelectedCells(2).Value
    End Sub
End Class
