﻿Imports System.Data.SqlClient
Public Class Cekout
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM cekout"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID"
        DataGridView1.Columns(1).HeaderText = "ID Cek In"
        DataGridView1.Columns(2).HeaderText = "Tanggal Cek IN"
        DataGridView1.Columns(3).HeaderText = "Tanggal Cek OUT"


        DataGridView1.Columns(0).Width = 200
        DataGridView1.Columns(1).Width = 200
        DataGridView1.Columns(2).Width = 195
        DataGridView1.Columns(3).Width = 185

    End Sub
    Sub bersih()
        txtid.Text = ""
        txtidcekin.Text = ""
        tglcekin.Text = ""
        tglcekout.Text = ""

    End Sub
    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_cekout FROM cekout"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_cekout").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtid.Text = "CT-00" + Trim(Str(kodeauto))
            Case 2 : txtid.Text = "CT-00" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub
    Sub id_cekin()
        Try
            Dim dt As New DataTable
            Dim ds As New DataSet
            ds.Tables.Add(dt)
            Dim da As New SqlDataAdapter("SELECT * FROM cekin", cn)
            da.Fill(dt)
            Dim r As DataRow
            txtidcekin.AutoCompleteCustomSource.Clear()
            For Each r In dt.Rows
                txtidcekin.AutoCompleteCustomSource.Add(r.Item(0).ToString)
            Next
        Catch ex As Exception
            cn.Close()
        End Try
    End Sub
    Private Sub Cekout_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_22_P2;Integrated Security=True"
        tampildata()
        txtid.Enabled = False
        kodeotomatis()
        id_cekin()
        tglcekin.Enabled = False
    End Sub

    Private Sub txtidpesan_TextChanged(sender As Object, e As EventArgs) Handles txtidcekin.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM cekin WHERE id_cekin = '" & txtidcekin.Text & "'"
        cmd.ExecuteNonQuery()
        Dim rd As SqlDataReader = cmd.ExecuteReader
        rd.Read()
        If rd.HasRows Then
            tglcekin.Text = rd.Item("tgl_cekin")
        Else
        End If
        rd.Close()
        cn.Close()
    End Sub


    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtid.Text = DataGridView1.SelectedCells(0).Value
        txtidcekin.Text = DataGridView1.SelectedCells(1).Value
        tglcekin.Value = DataGridView1.SelectedCells(2).Value
        tglcekout.Value = DataGridView1.SelectedCells(3).Value
    End Sub

    Private Sub Simpan_Click(sender As Object, e As EventArgs) Handles Simpan.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Cekout tidak boleh dikosongkan")
        ElseIf txtidcekin.Text = "" Then
            MessageBox.Show("ID Pemesanan tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtidcekin.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO cekout VALUES ('" & txtid.Text & "','" & txtidcekin.Text & "','" & tglcekin.Value.ToString("yyyy-MM-dd") & "','" & tglcekout.Value.ToString("yyyy-MM-dd") & " ')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Cek OUT Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Ubah_Click(sender As Object, e As EventArgs) Handles Ubah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Cek OUT diisi, tidak boleh dikosongkan")
        ElseIf txtidcekin.Text = "" Then
            MessageBox.Show("ID Cek IN wajib diisi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtidcekin.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE cekout SET id_cekin ='" & txtidcekin.Text & "',tgl_cekin ='" & tglcekin.Value.ToString("yyyy-MM-dd") & "', tgl_cekout ='" & tglcekout.Value.ToString("yyyy-MM-dd") & "' WHERE id_cekout ='" & txtid.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Cek OUT Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Hapus_Click(sender As Object, e As EventArgs) Handles Hapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM cekout WHERE id_cekout ='" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Cek OUT Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub Batal_Click(sender As Object, e As EventArgs) Handles Batal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub Keluar_Click(sender As Object, e As EventArgs) Handles Keluar.Click
        Me.Close()
        FrmMenu.Show()
    End Sub

    Private Sub txtcari_TextChanged(sender As Object, e As EventArgs) Handles txtcari.TextChanged
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM cekout WHERE id_cekout LIKE '%" & txtcari.Text & "%' OR id_cekin LIKE '%" & txtcari.Text & "%' OR tgl_cekin LIKE '%" & txtcari.Text & "%' OR tgl_cekout LIKE '%" & txtcari.Text & "%'"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()
    End Sub

    
    Private Sub Laporan_Click(sender As Object, e As EventArgs) Handles Laporan.Click
        Dim cekout As New Laporanout
        Laporanout.Show()
    End Sub
End Class