﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Cekout
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Laporan = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.tglcekin = New System.Windows.Forms.DateTimePicker()
        Me.tglcekout = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.txtidcekin = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Keluar = New System.Windows.Forms.Button()
        Me.txtcari = New System.Windows.Forms.TextBox()
        Me.Batal = New System.Windows.Forms.Button()
        Me.Hapus = New System.Windows.Forms.Button()
        Me.Ubah = New System.Windows.Forms.Button()
        Me.Simpan = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtid = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Laporan
        '
        Me.Laporan.BackColor = System.Drawing.Color.Navy
        Me.Laporan.FlatAppearance.BorderSize = 0
        Me.Laporan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Laporan.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Laporan.ForeColor = System.Drawing.Color.White
        Me.Laporan.Location = New System.Drawing.Point(748, 207)
        Me.Laporan.Margin = New System.Windows.Forms.Padding(2)
        Me.Laporan.Name = "Laporan"
        Me.Laporan.Size = New System.Drawing.Size(78, 28)
        Me.Laporan.TabIndex = 123
        Me.Laporan.Text = "Laporan"
        Me.Laporan.UseVisualStyleBackColor = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(247, 94)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(217, 13)
        Me.Label8.TabIndex = 122
        Me.Label8.Text = "*Tekan huruf c untuk menampilkan ID Cekin"
        '
        'tglcekin
        '
        Me.tglcekin.Location = New System.Drawing.Point(120, 120)
        Me.tglcekin.Margin = New System.Windows.Forms.Padding(2)
        Me.tglcekin.Name = "tglcekin"
        Me.tglcekin.Size = New System.Drawing.Size(188, 20)
        Me.tglcekin.TabIndex = 121
        '
        'tglcekout
        '
        Me.tglcekout.Location = New System.Drawing.Point(120, 152)
        Me.tglcekout.Margin = New System.Windows.Forms.Padding(2)
        Me.tglcekout.Name = "tglcekout"
        Me.tglcekout.Size = New System.Drawing.Size(188, 20)
        Me.tglcekout.TabIndex = 120
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Red
        Me.Label7.Location = New System.Drawing.Point(13, 152)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(99, 17)
        Me.Label7.TabIndex = 119
        Me.Label7.Text = "Tanggal Cek Out"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(13, 121)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 17)
        Me.Label2.TabIndex = 118
        Me.Label2.Text = "Tanggal Cek IN"
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(5, 241)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(2)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(821, 292)
        Me.DataGridView1.TabIndex = 117
        '
        'txtidcekin
        '
        Me.txtidcekin.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtidcekin.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtidcekin.Location = New System.Drawing.Point(120, 92)
        Me.txtidcekin.Margin = New System.Windows.Forms.Padding(2)
        Me.txtidcekin.Name = "txtidcekin"
        Me.txtidcekin.Size = New System.Drawing.Size(104, 20)
        Me.txtidcekin.TabIndex = 116
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(13, 92)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(58, 17)
        Me.Label6.TabIndex = 115
        Me.Label6.Text = "ID Cek In"
        '
        'Keluar
        '
        Me.Keluar.BackColor = System.Drawing.Color.Brown
        Me.Keluar.FlatAppearance.BorderSize = 0
        Me.Keluar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Keluar.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Keluar.ForeColor = System.Drawing.Color.White
        Me.Keluar.Location = New System.Drawing.Point(663, 207)
        Me.Keluar.Margin = New System.Windows.Forms.Padding(2)
        Me.Keluar.Name = "Keluar"
        Me.Keluar.Size = New System.Drawing.Size(78, 28)
        Me.Keluar.TabIndex = 114
        Me.Keluar.Text = "Kembali"
        Me.Keluar.UseVisualStyleBackColor = False
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(120, 207)
        Me.txtcari.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(204, 20)
        Me.txtcari.TabIndex = 113
        '
        'Batal
        '
        Me.Batal.BackColor = System.Drawing.Color.Brown
        Me.Batal.FlatAppearance.BorderSize = 0
        Me.Batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Batal.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Batal.ForeColor = System.Drawing.Color.White
        Me.Batal.Location = New System.Drawing.Point(581, 207)
        Me.Batal.Margin = New System.Windows.Forms.Padding(2)
        Me.Batal.Name = "Batal"
        Me.Batal.Size = New System.Drawing.Size(78, 28)
        Me.Batal.TabIndex = 112
        Me.Batal.Text = "Batal"
        Me.Batal.UseVisualStyleBackColor = False
        '
        'Hapus
        '
        Me.Hapus.BackColor = System.Drawing.Color.Brown
        Me.Hapus.FlatAppearance.BorderSize = 0
        Me.Hapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Hapus.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hapus.ForeColor = System.Drawing.Color.White
        Me.Hapus.Location = New System.Drawing.Point(499, 207)
        Me.Hapus.Margin = New System.Windows.Forms.Padding(2)
        Me.Hapus.Name = "Hapus"
        Me.Hapus.Size = New System.Drawing.Size(78, 28)
        Me.Hapus.TabIndex = 111
        Me.Hapus.Text = "Hapus"
        Me.Hapus.UseVisualStyleBackColor = False
        '
        'Ubah
        '
        Me.Ubah.BackColor = System.Drawing.Color.Brown
        Me.Ubah.FlatAppearance.BorderSize = 0
        Me.Ubah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Ubah.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ubah.ForeColor = System.Drawing.Color.White
        Me.Ubah.Location = New System.Drawing.Point(416, 207)
        Me.Ubah.Margin = New System.Windows.Forms.Padding(2)
        Me.Ubah.Name = "Ubah"
        Me.Ubah.Size = New System.Drawing.Size(78, 28)
        Me.Ubah.TabIndex = 110
        Me.Ubah.Text = "Ubah"
        Me.Ubah.UseVisualStyleBackColor = False
        '
        'Simpan
        '
        Me.Simpan.BackColor = System.Drawing.Color.Brown
        Me.Simpan.FlatAppearance.BorderSize = 0
        Me.Simpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Simpan.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Simpan.ForeColor = System.Drawing.Color.White
        Me.Simpan.Location = New System.Drawing.Point(333, 207)
        Me.Simpan.Margin = New System.Windows.Forms.Padding(2)
        Me.Simpan.Name = "Simpan"
        Me.Simpan.Size = New System.Drawing.Size(78, 28)
        Me.Simpan.TabIndex = 109
        Me.Simpan.Text = "Simpan"
        Me.Simpan.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Brown
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label5.Font = New System.Drawing.Font("Calibri", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(385, 8)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(73, 25)
        Me.Label5.TabIndex = 108
        Me.Label5.Text = "Cek Out"
        '
        'txtid
        '
        Me.txtid.Location = New System.Drawing.Point(120, 60)
        Me.txtid.Margin = New System.Windows.Forms.Padding(2)
        Me.txtid.Name = "txtid"
        Me.txtid.Size = New System.Drawing.Size(76, 20)
        Me.txtid.TabIndex = 107
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(9, 207)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 17)
        Me.Label4.TabIndex = 106
        Me.Label4.Text = "Pencarian Data"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(13, 60)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 17)
        Me.Label1.TabIndex = 105
        Me.Label1.Text = "ID"
        '
        'Cekout
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(830, 541)
        Me.Controls.Add(Me.Laporan)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tglcekin)
        Me.Controls.Add(Me.tglcekout)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.txtidcekin)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Keluar)
        Me.Controls.Add(Me.txtcari)
        Me.Controls.Add(Me.Batal)
        Me.Controls.Add(Me.Hapus)
        Me.Controls.Add(Me.Ubah)
        Me.Controls.Add(Me.Simpan)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtid)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Cekout"
        Me.Text = "Cekout"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Laporan As System.Windows.Forms.Button
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tglcekin As System.Windows.Forms.DateTimePicker
    Friend WithEvents tglcekout As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents txtidcekin As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Keluar As System.Windows.Forms.Button
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Batal As System.Windows.Forms.Button
    Friend WithEvents Hapus As System.Windows.Forms.Button
    Friend WithEvents Ubah As System.Windows.Forms.Button
    Friend WithEvents Simpan As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtid As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
