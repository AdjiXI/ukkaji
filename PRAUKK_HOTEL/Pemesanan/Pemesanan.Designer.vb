﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pemesanan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Laporan = New System.Windows.Forms.Button()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txttipe = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtnama = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtidkamar = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Keluar = New System.Windows.Forms.Button()
        Me.txtcari = New System.Windows.Forms.TextBox()
        Me.Batal = New System.Windows.Forms.Button()
        Me.Hapus = New System.Windows.Forms.Button()
        Me.Ubah = New System.Windows.Forms.Button()
        Me.Simpan = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txttamu = New System.Windows.Forms.TextBox()
        Me.txttelp = New System.Windows.Forms.TextBox()
        Me.txtid = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Laporan
        '
        Me.Laporan.BackColor = System.Drawing.Color.Navy
        Me.Laporan.FlatAppearance.BorderSize = 0
        Me.Laporan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Laporan.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Laporan.ForeColor = System.Drawing.Color.White
        Me.Laporan.Location = New System.Drawing.Point(748, 215)
        Me.Laporan.Margin = New System.Windows.Forms.Padding(2)
        Me.Laporan.Name = "Laporan"
        Me.Laporan.Size = New System.Drawing.Size(78, 28)
        Me.Laporan.TabIndex = 94
        Me.Laporan.Text = "Laporan"
        Me.Laporan.UseVisualStyleBackColor = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Red
        Me.Label9.Location = New System.Drawing.Point(207, 157)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(220, 13)
        Me.Label9.TabIndex = 93
        Me.Label9.Text = "*Tekan huruf k untuk menampilkan ID Kamar"
        '
        'txttipe
        '
        Me.txttipe.Location = New System.Drawing.Point(117, 186)
        Me.txttipe.Margin = New System.Windows.Forms.Padding(2)
        Me.txttipe.Name = "txttipe"
        Me.txttipe.Size = New System.Drawing.Size(144, 20)
        Me.txttipe.TabIndex = 92
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Red
        Me.Label8.Location = New System.Drawing.Point(10, 186)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 17)
        Me.Label8.TabIndex = 91
        Me.Label8.Text = "Tipe Kamar"
        '
        'txtnama
        '
        Me.txtnama.Location = New System.Drawing.Point(117, 67)
        Me.txtnama.Margin = New System.Windows.Forms.Padding(2)
        Me.txtnama.Name = "txtnama"
        Me.txtnama.Size = New System.Drawing.Size(204, 20)
        Me.txtnama.TabIndex = 90
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Red
        Me.Label7.Location = New System.Drawing.Point(10, 67)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(96, 17)
        Me.Label7.TabIndex = 89
        Me.Label7.Text = "Nama Pemesan"
        '
        'txtidkamar
        '
        Me.txtidkamar.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend
        Me.txtidkamar.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource
        Me.txtidkamar.Location = New System.Drawing.Point(117, 155)
        Me.txtidkamar.Margin = New System.Windows.Forms.Padding(2)
        Me.txtidkamar.Name = "txtidkamar"
        Me.txtidkamar.Size = New System.Drawing.Size(76, 20)
        Me.txtidkamar.TabIndex = 88
        '
        'DataGridView1
        '
        Me.DataGridView1.BackgroundColor = System.Drawing.Color.White
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(5, 247)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(2)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.Size = New System.Drawing.Size(820, 278)
        Me.DataGridView1.TabIndex = 87
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Location = New System.Drawing.Point(10, 155)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(61, 17)
        Me.Label6.TabIndex = 86
        Me.Label6.Text = "ID Kamar"
        '
        'Keluar
        '
        Me.Keluar.BackColor = System.Drawing.Color.Brown
        Me.Keluar.FlatAppearance.BorderSize = 0
        Me.Keluar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Keluar.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Keluar.ForeColor = System.Drawing.Color.White
        Me.Keluar.Location = New System.Drawing.Point(666, 215)
        Me.Keluar.Margin = New System.Windows.Forms.Padding(2)
        Me.Keluar.Name = "Keluar"
        Me.Keluar.Size = New System.Drawing.Size(78, 28)
        Me.Keluar.TabIndex = 85
        Me.Keluar.Text = "Kembali"
        Me.Keluar.UseVisualStyleBackColor = False
        '
        'txtcari
        '
        Me.txtcari.Location = New System.Drawing.Point(117, 221)
        Me.txtcari.Margin = New System.Windows.Forms.Padding(2)
        Me.txtcari.Name = "txtcari"
        Me.txtcari.Size = New System.Drawing.Size(204, 20)
        Me.txtcari.TabIndex = 84
        '
        'Batal
        '
        Me.Batal.BackColor = System.Drawing.Color.Brown
        Me.Batal.FlatAppearance.BorderSize = 0
        Me.Batal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Batal.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Batal.ForeColor = System.Drawing.Color.White
        Me.Batal.Location = New System.Drawing.Point(583, 215)
        Me.Batal.Margin = New System.Windows.Forms.Padding(2)
        Me.Batal.Name = "Batal"
        Me.Batal.Size = New System.Drawing.Size(78, 28)
        Me.Batal.TabIndex = 83
        Me.Batal.Text = "Batal"
        Me.Batal.UseVisualStyleBackColor = False
        '
        'Hapus
        '
        Me.Hapus.BackColor = System.Drawing.Color.Brown
        Me.Hapus.FlatAppearance.BorderSize = 0
        Me.Hapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Hapus.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Hapus.ForeColor = System.Drawing.Color.White
        Me.Hapus.Location = New System.Drawing.Point(500, 215)
        Me.Hapus.Margin = New System.Windows.Forms.Padding(2)
        Me.Hapus.Name = "Hapus"
        Me.Hapus.Size = New System.Drawing.Size(78, 28)
        Me.Hapus.TabIndex = 82
        Me.Hapus.Text = "Hapus"
        Me.Hapus.UseVisualStyleBackColor = False
        '
        'Ubah
        '
        Me.Ubah.BackColor = System.Drawing.Color.Brown
        Me.Ubah.FlatAppearance.BorderSize = 0
        Me.Ubah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Ubah.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Ubah.ForeColor = System.Drawing.Color.White
        Me.Ubah.Location = New System.Drawing.Point(418, 215)
        Me.Ubah.Margin = New System.Windows.Forms.Padding(2)
        Me.Ubah.Name = "Ubah"
        Me.Ubah.Size = New System.Drawing.Size(78, 28)
        Me.Ubah.TabIndex = 81
        Me.Ubah.Text = "Ubah"
        Me.Ubah.UseVisualStyleBackColor = False
        '
        'Simpan
        '
        Me.Simpan.BackColor = System.Drawing.Color.Brown
        Me.Simpan.FlatAppearance.BorderSize = 0
        Me.Simpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Simpan.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Simpan.ForeColor = System.Drawing.Color.White
        Me.Simpan.Location = New System.Drawing.Point(336, 215)
        Me.Simpan.Margin = New System.Windows.Forms.Padding(2)
        Me.Simpan.Name = "Simpan"
        Me.Simpan.Size = New System.Drawing.Size(78, 28)
        Me.Simpan.TabIndex = 80
        Me.Simpan.Text = "Simpan"
        Me.Simpan.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Brown
        Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Label5.Font = New System.Drawing.Font("Calibri", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(347, 9)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(101, 25)
        Me.Label5.TabIndex = 79
        Me.Label5.Text = "Pemesanan"
        '
        'txttamu
        '
        Me.txttamu.Location = New System.Drawing.Point(117, 126)
        Me.txttamu.Margin = New System.Windows.Forms.Padding(2)
        Me.txttamu.Name = "txttamu"
        Me.txttamu.Size = New System.Drawing.Size(204, 20)
        Me.txttamu.TabIndex = 78
        '
        'txttelp
        '
        Me.txttelp.Location = New System.Drawing.Point(117, 95)
        Me.txttelp.Margin = New System.Windows.Forms.Padding(2)
        Me.txttelp.Name = "txttelp"
        Me.txttelp.Size = New System.Drawing.Size(144, 20)
        Me.txttelp.TabIndex = 77
        '
        'txtid
        '
        Me.txtid.Location = New System.Drawing.Point(117, 37)
        Me.txtid.Margin = New System.Windows.Forms.Padding(2)
        Me.txtid.Name = "txtid"
        Me.txtid.Size = New System.Drawing.Size(76, 20)
        Me.txtid.TabIndex = 76
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(10, 221)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(95, 17)
        Me.Label4.TabIndex = 75
        Me.Label4.Text = "Pencarian Data"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.Location = New System.Drawing.Point(10, 95)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(50, 17)
        Me.Label3.TabIndex = 74
        Me.Label3.Text = "No Telp"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(10, 126)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 17)
        Me.Label2.TabIndex = 73
        Me.Label2.Text = "Nama Tamu"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(10, 37)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(21, 17)
        Me.Label1.TabIndex = 72
        Me.Label1.Text = "ID"
        '
        'Pemesanan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ControlLight
        Me.ClientSize = New System.Drawing.Size(830, 541)
        Me.Controls.Add(Me.Laporan)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txttipe)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.txtnama)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtidkamar)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Keluar)
        Me.Controls.Add(Me.txtcari)
        Me.Controls.Add(Me.Batal)
        Me.Controls.Add(Me.Hapus)
        Me.Controls.Add(Me.Ubah)
        Me.Controls.Add(Me.Simpan)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txttamu)
        Me.Controls.Add(Me.txttelp)
        Me.Controls.Add(Me.txtid)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Pemesanan"
        Me.Text = "Pemesanan"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Laporan As System.Windows.Forms.Button
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txttipe As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtnama As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtidkamar As System.Windows.Forms.TextBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Keluar As System.Windows.Forms.Button
    Friend WithEvents txtcari As System.Windows.Forms.TextBox
    Friend WithEvents Batal As System.Windows.Forms.Button
    Friend WithEvents Hapus As System.Windows.Forms.Button
    Friend WithEvents Ubah As System.Windows.Forms.Button
    Friend WithEvents Simpan As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txttamu As System.Windows.Forms.TextBox
    Friend WithEvents txttelp As System.Windows.Forms.TextBox
    Friend WithEvents txtid As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
