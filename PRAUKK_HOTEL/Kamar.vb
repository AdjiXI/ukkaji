﻿Imports System.Data.SqlClient
Public Class Kamar
    Dim cn As New SqlConnection
    Dim cmd As New SqlCommand
    Sub tampildata()
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT * FROM kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        Dim dt As New DataTable
        dt.Load(rd)
        DataGridView1.DataSource = dt
        cn.Close()

        DataGridView1.Columns(0).HeaderText = "ID "
        DataGridView1.Columns(1).HeaderText = "Tipe Kamar"
        DataGridView1.Columns(2).HeaderText = "Jumlah"

        DataGridView1.Columns(0).Width = 150
        DataGridView1.Columns(1).Width = 415
        DataGridView1.Columns(2).Width = 210
    End Sub
    Sub kodeotomatis()
        Dim kodeauto As Single
        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "SELECT COUNT(*) AS id_kamar FROM kamar"
        Dim rd As SqlDataReader = cmd.ExecuteReader
        While rd.Read
            kodeauto = Val(rd.Item("id_kamar").ToString) + 1
        End While
        Select Case Len(Trim(kodeauto))
            Case 1 : txtid.Text = "K-00" + Trim(Str(kodeauto))
            Case 2 : txtid.Text = "K-00" + Trim(Str(kodeauto))
        End Select
        rd.Close()
        cn.Close()
    End Sub
    Sub bersih()
        txtid.Text = ""
        txtkamar.Text = ""
        txtjml.Text = ""
    End Sub

    Private Sub Kamar_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cn.ConnectionString = "Data Source=DESKTOP-GCB5DI9\SQLEXPRESS;Initial Catalog=db_UKK_22_P2;Integrated Security=True"
        tampildata()
        txtid.Enabled = False
        kodeotomatis()
    End Sub

    Private Sub Simpan_Click(sender As Object, e As EventArgs) Handles Simpan.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Kamar, tidak boleh dikosongkan")
        ElseIf txtkamar.Text = "" Then
            MessageBox.Show("Tipe Kamar di isi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtkamar.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "INSERT INTO kamar VALUES ('" & txtid.Text & "','" & txtkamar.Text & "','" & txtjml.Text & " ')"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Kamar Berhasil Tersimpan", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Ubah_Click(sender As Object, e As EventArgs) Handles Ubah.Click
        If txtid.Text = "" Then
            MessageBox.Show("ID Kamar, tidak boleh dikosongkan")
        ElseIf txtkamar.Text = "" Then
            MessageBox.Show("Tipe Kamar wajib di isi, tidak boleh dikosongkan")
        ElseIf txtid.Text <> "" And txtkamar.Text <> "" Then
            cn.Open()
            cmd.Connection = cn
            cmd.CommandText = "UPDATE kamar SET tipe_kamar ='" & txtkamar.Text & "', jumlah ='" & txtjml.Text & "' WHERE id_kamar = '" & txtid.Text & "'"
            cmd.ExecuteNonQuery()
            cn.Close()
            bersih()
            MsgBox("Data Kamar Berhasil Terubah", MsgBoxStyle.Information)
            tampildata()
            kodeotomatis()
        End If
    End Sub

    Private Sub Hapus_Click(sender As Object, e As EventArgs) Handles Hapus.Click
        Dim baris As Integer
        Dim id As String

        baris = DataGridView1.CurrentCell.RowIndex
        id = DataGridView1(0, baris).Value.ToString

        cn.Open()
        cmd.Connection = cn
        cmd.CommandText = "DELETE FROM kamar WHERE id_kamar = '" + id + "'"
        cmd.ExecuteNonQuery()
        cn.Close()
        MsgBox("Data Kamar Berhasil Terhapus", MsgBoxStyle.Information)
        tampildata()
        kodeotomatis()
    End Sub

    Private Sub Batal_Click(sender As Object, e As EventArgs) Handles Batal.Click
        bersih()
        kodeotomatis()
    End Sub

    Private Sub Keluar_Click(sender As Object, e As EventArgs) Handles Keluar.Click
        Me.Close()
        FrmMenu.Show()
    End Sub

    Private Sub DataGridView1_DoubleClick1(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        txtid.Text = DataGridView1.SelectedCells(0).Value
        txtkamar.Text = DataGridView1.SelectedCells(1).Value
        txtjml.Text = DataGridView1.SelectedCells(2).Value
    End Sub
End Class