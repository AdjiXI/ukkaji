USE [master]
GO
/****** Object:  Database [db_UKK_22_P2]    Script Date: 5/26/2022 8:15:20 PM ******/
CREATE DATABASE [db_UKK_22_P2]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'pra_ukk', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\pra_ukk.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'pra_ukk_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\pra_ukk_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [db_UKK_22_P2] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [db_UKK_22_P2].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [db_UKK_22_P2] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET ARITHABORT OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [db_UKK_22_P2] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [db_UKK_22_P2] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET  DISABLE_BROKER 
GO
ALTER DATABASE [db_UKK_22_P2] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [db_UKK_22_P2] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [db_UKK_22_P2] SET  MULTI_USER 
GO
ALTER DATABASE [db_UKK_22_P2] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [db_UKK_22_P2] SET DB_CHAINING OFF 
GO
ALTER DATABASE [db_UKK_22_P2] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [db_UKK_22_P2] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [db_UKK_22_P2] SET DELAYED_DURABILITY = DISABLED 
GO
USE [db_UKK_22_P2]
GO
/****** Object:  Table [dbo].[cekin]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cekin](
	[id_cekin] [char](10) NOT NULL,
	[id_pemesanan] [char](10) NULL,
	[nama_pemesanan] [varchar](50) NULL,
	[tgl_cekin] [datetime] NULL,
 CONSTRAINT [PK_cekin] PRIMARY KEY CLUSTERED 
(
	[id_cekin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cekout]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cekout](
	[id_cekout] [char](10) NULL,
	[id_cekin] [char](10) NULL,
	[tgl_cekin] [datetime] NULL,
	[tgl_cekout] [datetime] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fasilitas_hotel]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fasilitas_hotel](
	[id_hotel] [char](10) NOT NULL,
	[fasilitas] [varchar](50) NULL,
	[keterangan] [varchar](50) NULL,
 CONSTRAINT [PK_fasilitas_hotel] PRIMARY KEY CLUSTERED 
(
	[id_hotel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[fasilitas_kamar]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[fasilitas_kamar](
	[id_fasilitas_kamar] [char](10) NOT NULL,
	[id_kamar] [char](10) NULL,
	[tipe_kamar] [varchar](50) NULL,
	[fasilitas] [varchar](50) NULL,
 CONSTRAINT [PK_Fasiltas_kamar] PRIMARY KEY CLUSTERED 
(
	[id_fasilitas_kamar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[kamar]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[kamar](
	[id_kamar] [char](10) NOT NULL,
	[tipe_kamar] [varchar](50) NULL,
	[jumlah] [varchar](50) NULL,
 CONSTRAINT [PK_Kamar] PRIMARY KEY CLUSTERED 
(
	[id_kamar] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[login]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[login](
	[username] [varchar](50) NOT NULL,
	[password] [varchar](50) NULL,
	[hak] [varchar](50) NULL,
 CONSTRAINT [PK_login] PRIMARY KEY CLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pemesan]    Script Date: 5/26/2022 8:15:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pemesan](
	[id_pemesan] [char](10) NOT NULL,
	[nama_pemesan] [varchar](50) NULL,
	[no_telp] [varchar](50) NULL,
	[nama_tamu] [varchar](50) NULL,
	[id_kamar] [char](10) NULL,
	[tipe_kamar] [varchar](50) NULL,
 CONSTRAINT [PK_pemesan] PRIMARY KEY CLUSTERED 
(
	[id_pemesan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[cekin] ([id_cekin], [id_pemesanan], [nama_pemesanan], [tgl_cekin]) VALUES (N'CN-001    ', N'P-001     ', N'Aji', CAST(N'2022-04-27 00:00:00.000' AS DateTime))
INSERT [dbo].[cekin] ([id_cekin], [id_pemesanan], [nama_pemesanan], [tgl_cekin]) VALUES (N'CN-002    ', N'P-002     ', N'Jizzy', CAST(N'2022-04-26 00:00:00.000' AS DateTime))
INSERT [dbo].[cekin] ([id_cekin], [id_pemesanan], [nama_pemesanan], [tgl_cekin]) VALUES (N'CN-003    ', N'P-003     ', N'Alfonsus', CAST(N'2022-04-28 00:00:00.000' AS DateTime))
INSERT [dbo].[cekin] ([id_cekin], [id_pemesanan], [nama_pemesanan], [tgl_cekin]) VALUES (N'CN-004    ', N'P-003     ', N'Alfonsus', CAST(N'2022-05-11 00:00:00.000' AS DateTime))
INSERT [dbo].[cekout] ([id_cekout], [id_cekin], [tgl_cekin], [tgl_cekout]) VALUES (N'CT-001    ', N'CN-001    ', CAST(N'2022-04-27 00:00:00.000' AS DateTime), CAST(N'2022-04-28 00:00:00.000' AS DateTime))
INSERT [dbo].[cekout] ([id_cekout], [id_cekin], [tgl_cekin], [tgl_cekout]) VALUES (N'CT-002    ', N'CN-002    ', CAST(N'2022-04-26 00:00:00.000' AS DateTime), CAST(N'2022-04-27 00:00:00.000' AS DateTime))
INSERT [dbo].[cekout] ([id_cekout], [id_cekin], [tgl_cekin], [tgl_cekout]) VALUES (N'CT-003    ', N'CN-003    ', CAST(N'2022-04-28 00:00:00.000' AS DateTime), CAST(N'2022-04-29 00:00:00.000' AS DateTime))
INSERT [dbo].[cekout] ([id_cekout], [id_cekin], [tgl_cekin], [tgl_cekout]) VALUES (N'CT-004    ', N'CN-003    ', CAST(N'2022-04-28 00:00:00.000' AS DateTime), CAST(N'2022-05-12 00:00:00.000' AS DateTime))
INSERT [dbo].[fasilitas_hotel] ([id_hotel], [fasilitas], [keterangan]) VALUES (N'H-001     ', N'GYM', N'Berada Di Lantai 3 ')
INSERT [dbo].[fasilitas_hotel] ([id_hotel], [fasilitas], [keterangan]) VALUES (N'H-002     ', N'Swimming Pool', N'Berada Di Lantai Atas  ')
INSERT [dbo].[fasilitas_hotel] ([id_hotel], [fasilitas], [keterangan]) VALUES (N'H-003     ', N'Muhsola', N'Lantai 3 ')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas_kamar], [id_kamar], [tipe_kamar], [fasilitas]) VALUES (N'FK-001    ', N'K-001     ', N'Delux', N'Twin Bed, AC, TV LED ')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas_kamar], [id_kamar], [tipe_kamar], [fasilitas]) VALUES (N'FK-002    ', N'K-002     ', N'Superior', N'Twin Bed, Balkon View, Frezer ')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas_kamar], [id_kamar], [tipe_kamar], [fasilitas]) VALUES (N'FK-003    ', N'K-003     ', N'Presendetian', N'Coffe Maker, Mini Bar, Dapur Mini ')
INSERT [dbo].[fasilitas_kamar] ([id_fasilitas_kamar], [id_kamar], [tipe_kamar], [fasilitas]) VALUES (N'FK-004    ', N'K-002     ', N'Superior', N'Tv 32 inc ')
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah]) VALUES (N'K-001     ', N'Delux', N'15 ')
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah]) VALUES (N'K-002     ', N'Superior', N'20 ')
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah]) VALUES (N'K-003     ', N'Presendetian', N'10 ')
INSERT [dbo].[kamar] ([id_kamar], [tipe_kamar], [jumlah]) VALUES (N'K-004     ', N'Standrat', N'15 ')
INSERT [dbo].[login] ([username], [password], [hak]) VALUES (N'admin', N'admin', N'admin')
INSERT [dbo].[login] ([username], [password], [hak]) VALUES (N'aji', N'aji', N'resepsionis')
INSERT [dbo].[pemesan] ([id_pemesan], [nama_pemesan], [no_telp], [nama_tamu], [id_kamar], [tipe_kamar]) VALUES (N'P-001     ', N'Aji', N'082150258737', N'Rizky', N'K-001     ', N'Delux ')
INSERT [dbo].[pemesan] ([id_pemesan], [nama_pemesan], [no_telp], [nama_tamu], [id_kamar], [tipe_kamar]) VALUES (N'P-002     ', N'Jizzy', N'082150258737', N'Pokemon', N'K-002     ', N'Superior ')
INSERT [dbo].[pemesan] ([id_pemesan], [nama_pemesan], [no_telp], [nama_tamu], [id_kamar], [tipe_kamar]) VALUES (N'P-003     ', N'Alfonsus', N'082150258737', N'Hervian', N'K-003     ', N'Presendetian ')
INSERT [dbo].[pemesan] ([id_pemesan], [nama_pemesan], [no_telp], [nama_tamu], [id_kamar], [tipe_kamar]) VALUES (N'P-004     ', N'Aji Rizki', N'08215025837', N'Emon', N'K-002     ', N'Superior ')
ALTER TABLE [dbo].[cekin]  WITH CHECK ADD  CONSTRAINT [FK_cekin_pemesan] FOREIGN KEY([id_pemesanan])
REFERENCES [dbo].[pemesan] ([id_pemesan])
GO
ALTER TABLE [dbo].[cekin] CHECK CONSTRAINT [FK_cekin_pemesan]
GO
ALTER TABLE [dbo].[cekout]  WITH CHECK ADD  CONSTRAINT [FK_cekout_cekin] FOREIGN KEY([id_cekin])
REFERENCES [dbo].[cekin] ([id_cekin])
GO
ALTER TABLE [dbo].[cekout] CHECK CONSTRAINT [FK_cekout_cekin]
GO
ALTER TABLE [dbo].[fasilitas_kamar]  WITH CHECK ADD  CONSTRAINT [FK_fasilitas_kamar_kamar] FOREIGN KEY([id_kamar])
REFERENCES [dbo].[kamar] ([id_kamar])
GO
ALTER TABLE [dbo].[fasilitas_kamar] CHECK CONSTRAINT [FK_fasilitas_kamar_kamar]
GO
USE [master]
GO
ALTER DATABASE [db_UKK_22_P2] SET  READ_WRITE 
GO
